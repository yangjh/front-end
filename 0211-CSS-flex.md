# Flexbox

<div class="figure" style="text-align: center">
<img src="images/css-flexbox.png" alt="Flex box布局"  />
<p class="caption">(\#fig:flexbox)Flex box布局</p>
</div>

Flex box布局模型的主要目的是提供更有效率的布局方式，尤其是当容器内元素的尺寸不固定的时候更能表现出它的优势。它能够自动识别子元素的尺寸，从而为盒装模型提供更高的灵活性。

## 基本概念

如果元素采用Flex进行布局，那么这个元素就可以称为Flex容器（Flex container），元素的所有子元素称为Flex项目（Flex item）。

下图为Flexbox模型图：

<div class="figure" style="text-align: center">
<img src="images/CSS3-Flexbox-Model.jpg" alt="Flexbox模型图"  />
<p class="caption">(\#fig:flexboxmodel)Flexbox模型图</p>
</div>

### 几个术语

* main axis：水平主轴
* main-start：主轴开始位置，与边框的交叉点
* main-end：主轴的结束位置
* cross axis：垂直交叉轴
* cross-start：交叉轴的开始位置
* cross-end：交叉轴的结束位置
* main size：单个项目占据的主轴空间
* cross size：单个项目占据的交叉轴空间

## Flexbox容器的构造方法

1. 任何一个容器都可以指定为Flexbox布局：

```css
.flex-container {
  display: flex;
}
```

2. 行内元素可以指定Flexbox布局：

```css
.flex-container {
  display: inline-flex;
}
```

**注意，设为 Flex 布局以后，子元素的`float`、`clear`和`vertical-align`属性将失效。**

## Flexbox容器特性

### flex-direction属性

`flex-direction`属性决定主轴的方向（即项目的排列方向）。

```css
.flex-container {
  flex-direction: row | row-reverse | column | column-reverse;
}
```

#### Values:

```css
.flex-container {
  flex-direction:         row; /* 默认值 */
}
```

主轴为水平方向，起点在左端:

<div class="figure" style="text-align: center">
<img src="images/flex-direction-row.png" alt="flex-direction-row"  />
<p class="caption">(\#fig:flex-direction-row)flex-direction-row</p>
</div>

```css
.flex-container {
  flex-direction: row-reverse;
}
```

主轴为水平方向，起点在右端:

<div class="figure" style="text-align: center">
<img src="images/flex-direction-row-reverse.png" alt="flex-direction-row-reverse"  />
<p class="caption">(\#fig:flex-direction-row-reverse)flex-direction-row-reverse</p>
</div>


```css
.flex-container {
  flex-direction:         column;
}
```

主轴为垂直方向，起点在上端:

<div class="figure" style="text-align: center">
<img src="images/flex-direction-column.png" alt="flex-direction-column"  />
<p class="caption">(\#fig:flex-direction-column)flex-direction-column</p>
</div>

```css
.flex-container {
  flex-direction:         column-reverse;
}
```

主轴为垂直方向，起点在下端:

<div class="figure" style="text-align: center">
<img src="images/flex-direction-column-reverse.png" alt="flex-direction-column-reverse"  />
<p class="caption">(\#fig:flex-direction-column-reverse)flex-direction-column-reverse</p>
</div>

### flex-wrap属性
`flex-wrap`属性决定内容在抽线上排列不下的换行方式。

```css
.flex-container {
  flex-wrap: nowrap | wrap | wrap-reverse;
}
```
#### Values:

```css
.flex-container {
  flex-wrap:         nowrap; /* 默认 */
}
```

<div class="figure" style="text-align: center">
<img src="images/flex-wrap-nowrap.png" alt="flex-wrap-nowrap"  />
<p class="caption">(\#fig:flex-wrap-nowrap)flex-wrap-nowrap</p>
</div>

设置不换行。

```css
.flex-container {
  flex-wrap:         wrap;
}
```

<div class="figure" style="text-align: center">
<img src="images/flex-wrap-wrap.png" alt="flex-wrap-wrap"  />
<p class="caption">(\#fig:flex-wrap-wrap)flex-wrap-wrap</p>
</div>

设置自动换行，且第一行在上方。

```css
.flex-container {
  flex-wrap:         wrap-reverse;
}
```

<div class="figure" style="text-align: center">
<img src="images/flex-wrap-wrap-reverse.png" alt="flex-wrap-wrap-reverse"  />
<p class="caption">(\#fig:flex-wrap-wrap-reverse)flex-wrap-wrap-reverse</p>
</div>

设置自动换行，且第一行在下方。

### flex-flow属性

`flex-flow`属性是`flex-direction`属性和`flex-wrap`属性的简写形式。

#### **Values:**

```css
.flex-container {
  flex-flow:         <flex-direction> || <flex-wrap>;
}
```

默认属性值：

```css
.flex-container {
  flex-flow:         row nowrap;
}
```

### justify-content属性

`justify-content`属性定义项目在主轴上的对齐方式。

```css
.flex-container {
  justify-content: flex-start | flex-end | center | space-between | space-around;
}
```

#### Values:

```css
.flex-container {
  justify-content:         flex-start;
}
```

<div class="figure" style="text-align: center">
<img src="images/justify-content-flex-start.png" alt="justify-content-flex-start"  />
<p class="caption">(\#fig:justify-content-flex-start)justify-content-flex-start</p>
</div>

左对齐。

```css
.flex-container {
  justify-content:         flex-end;
}
```

<div class="figure" style="text-align: center">
<img src="images/justify-content-flex-end.png" alt="justify-content-flex-end"  />
<p class="caption">(\#fig:justify-content-flex-end)justify-content-flex-end</p>
</div>

右对齐。

```css
.flex-container {
  justify-content:         center;
}
```
<div class="figure" style="text-align: center">
<img src="images/justify-content-center.png" alt="justify-content-center"  />
<p class="caption">(\#fig:justify-content-center)justify-content-center</p>
</div>

居中。

```css
.flex-container {
  justify-content:         space-between;
}
```

<div class="figure" style="text-align: center">
<img src="images/justify-content-space-between.png" alt="justify-content-space-between"  />
<p class="caption">(\#fig:justify-content-space-between)justify-content-space-between</p>
</div>

两端对齐，项目之间的间隔相等。

```css
.flex-container {
  justify-content:         space-around;
}
```

<div class="figure" style="text-align: center">
<img src="images/justify-content-space-around.png" alt="justify-content-space-around"  />
<p class="caption">(\#fig:justify-content-space-around)justify-content-space-around</p>
</div>

每个项目两侧的间隔相等。

### align-items属性

`align-items`属性定义项目在交叉轴上的对齐方式。

```css
.flex-container {
  align-items: align-items: flex-start | flex-end | center | baseline | stretch;
}
```

#### Values:

```css
.flex-container {
  align-items:         flex-start;
}
```

<div class="figure" style="text-align: center">
<img src="images/align-items-flex-start.png" alt="align-items-flex-start"  />
<p class="caption">(\#fig:align-items-flex-start)align-items-flex-start</p>
</div>

交叉轴的起点对齐。

```css
.flex-container {
  align-items:         flex-end;
}
```

<div class="figure" style="text-align: center">
<img src="images/align-items-flex-end.png" alt="align-items-flex-end"  />
<p class="caption">(\#fig:align-items-flex-end)align-items-flex-end</p>
</div>

交叉轴的终点对齐。

```css
.flex-container {
  align-items:         center;
}
```

<div class="figure" style="text-align: center">
<img src="images/align-items-center.png" alt="align-items-center"  />
<p class="caption">(\#fig:align-items-center)align-items-center</p>
</div>

交叉轴的中点对齐。

```css
.flex-container {
  align-items:         baseline;
}
```

<div class="figure" style="text-align: center">
<img src="images/align-items-baseline.png" alt="align-items-baseline"  />
<p class="caption">(\#fig:align-items-baseline)align-items-baseline</p>
</div>

项目的第一行文字的基线对齐。

```css
.flex-container {
  align-items:         stretch;
}
```

<div class="figure" style="text-align: center">
<img src="images/align-items-stretch.png" alt="align-items-stretch"  />
<p class="caption">(\#fig:align-items-stretch)align-items-stretch</p>
</div>

如果项目未设置高度或设为auto，将占满整个容器的高度。

### align-content属性

`align-content`定义了多根轴线的对齐方式。如果项目只有一根轴线，该属性不起作用。

```css
.flex-container {
  align-content: flex-start | flex-end | center | space-between | space-around | stretch;
}
```

#### Values:

```css
.flex-container {
  align-content:         flex-start;
}
```

<div class="figure" style="text-align: center">
<img src="images/align-content-flex-start.png" alt="align-content-flex-start"  />
<p class="caption">(\#fig:align-content-flex-start)align-content-flex-start</p>
</div>

与交叉轴的起点对齐。

```css
.flex-container {
  align-content:         flex-end;
}
```

<div class="figure" style="text-align: center">
<img src="images/align-content-flex-end.png" alt="align-content-flex-end"  />
<p class="caption">(\#fig:align-content-flex-end)align-content-flex-end</p>
</div>

与交叉轴的终点对齐。

```css
.flex-container {
  align-content:         center;
}
```

<div class="figure" style="text-align: center">
<img src="images/align-content-center.png" alt="align-content-center"  />
<p class="caption">(\#fig:align-content-center)align-content-center</p>
</div>

与交叉轴的中点对齐。

```css
.flex-container {
  align-content:         space-between;
}
```

<div class="figure" style="text-align: center">
<img src="images/align-content-space-between.png" alt="align-content-space-between"  />
<p class="caption">(\#fig:align-content-space-between)align-content-space-between</p>
</div>

与交叉轴两端对齐，轴线之间的间隔平均分布。

```css
.flex-container {
  align-content:         space-around;
}
```

<div class="figure" style="text-align: center">
<img src="images/align-content-space-around.png" alt="align-content-space-around"  />
<p class="caption">(\#fig:align-content-space-around)align-content-space-around</p>
</div>

每根轴线两侧的间隔都相等。

```css
.flex-container {
  align-content:         stretch;
}
```

<div class="figure" style="text-align: center">
<img src="images/align-content-stretch.png" alt="align-content-stretch"  />
<p class="caption">(\#fig:align-content-stretch)align-content-stretch</p>
</div>

轴线占满整个交叉轴。

## Flexbox项目特性

### order属性

`order`属性定义项目的排列顺序。数值越小，排列越靠前，默认为`0`。
#### Values:

```css
.flex-item {
  order:         <integer>;
}
```

<div class="figure" style="text-align: center">
<img src="images/flex-order.png" alt="flex-order"  />
<p class="caption">(\#fig:flex-order)flex-order</p>
</div>

### flex-grow属性

`flex-grow`属性定义项目的放大比例，默认为`0`，即如果存在剩余空间，也不放大。

#### Values:

```css
.flex-item {
  flex-grow:         <number>;
}
```

<div class="figure" style="text-align: center">
<img src="images/flex-grow.png" alt="flex-grow"  />
<p class="caption">(\#fig:flex-grow)flex-grow</p>
</div>

如果所有项目的`flex-grow`属性都为`1`，则它们将等分剩余空间（如果有的话）。如果一个项目的`flex-grow`属性为`2`，其他项目都为`1`，则前者占据的剩余空间将比其他项多一倍。

### flex-shrink属性

`flex-shrink`属性定义了项目的缩小比例，默认为`1`，即如果空间不足，该项目将缩小。

#### Values:

```css
.flex-item {
  flex-shrink:         <number>;
}
```

<div class="figure" style="text-align: center">
<img src="images/flex-shrink.png" alt="flex-shrink"  />
<p class="caption">(\#fig:flex-shrink)flex-shrink</p>
</div>

如果所有项目的`flex-shrink`属性都为1，当空间不足时，都将等比例缩小。如果一个项目的`flex-shrink`属性为0，其他项目都为1，则空间不足时，前者不缩小。

负值对该属性无效。

### flex-basis属性

`flex-basis`属性定义了在分配多余空间之前，项目占据的主轴空间（main size）。浏览器根据这个属性，计算主轴是否有多余空间。它的默认值为`auto`，即项目的本来大小。

#### Values：

```css
.flex-item {
  flex-basis:         auto | <width>;
}
```

它可以设为跟`width`或`height`属性一样的值（比如350px），则项目将占据固定空间。

### flex属性

`flex`属性是`flex-grow`, `flex-shrink` 和 `flex-basis`的简写，默认值为`0 1 auto`。后两个属性可选。

#### Values：

```css
.flex-item {
  flex:         none | auto | [ <flex-grow> <flex-shrink>? || <flex-basis> ];
}
```

### align-self属性

`align-self`属性允许单个项目有与其他项目不一样的对齐方式，可覆盖`align-items`属性。默认值为`auto`，表示继承父元素的`align-items`属性，如果没有父元素，则等同于`stretch`。

#### Values：

```css
.flex-item {
  align-self:         auto | flex-start | flex-end | center | baseline | stretch;
}
```

<iframe
     src="https://codesandbox.io/embed/mystifying-ritchie-1sicb?fontsize=14&hidenavigation=1&theme=light"
     style="width:100%; height:500px; border:0; border-radius: 4px; overflow:hidden;"
     title="flexbox"
     allow="accelerometer; ambient-light-sensor; camera; encrypted-media; geolocation; gyroscope; hid; microphone; midi; payment; usb; vr"
     sandbox="allow-forms allow-modals allow-popups allow-presentation allow-same-origin allow-scripts"
   ></iframe>

## 参考资料

1. [https://www.w3.org/html/ig/zh/css-flex-1/#intro](https://www.w3.org/html/ig/zh/css-flex-1/#intro)
1. [http://www.ruanyifeng.com/blog/2015/07/flex-grammar.html)](http://www.ruanyifeng.com/blog/2015/07/flex-grammar.html)
1. [https://scotch.io/tutorials/a-visual-guide-to-css3-flexbox-properties](https://scotch.io/tutorials/a-visual-guide-to-css3-flexbox-properties)
1. [https://css-tricks.com/snippets/css/a-guide-to-flexbox/](https://css-tricks.com/snippets/css/a-guide-to-flexbox/)
1. [https://developer.mozilla.org/zh-CN/docs/Learn/CSS/CSS_layout/Flexbox](https://developer.mozilla.org/zh-CN/docs/Learn/CSS/CSS_layout/Flexbox)
