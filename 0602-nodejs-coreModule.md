# Node.js 常用核心模块

核心模块是被编译成二进制代码，引用的时候只需 require 即可。下面对 Node.js 中的核心模块，做简要介绍。

## 事件 Events

事件模块是最重要的核心模块，所有能够触发事件的对象，都是`EventEmitter`类的实例。通过这个模块，我们可以建立自己的事件监听、触发和移除机制。

挂接模块

```js
const EventEmitter = require('events')
```

实例化一个 EventEmitter 对象

```js
class MyEmitter extends EventEmitter {}
const myEmitter = new MyEmitter()
```

注册事件

```js
emitter.on( 'Event_Name' , callBack_Fun )
emitter.once( 'Event_Name' , callBack_Fun ) //注册一个单次监听器，触发一次后立刻解除
```

触发事件

```js
event.emit('Event_Name' , 参数 1, 参数 2);
```

移除事件

```js
emitter…removeListener(‘Event_Name’ , callBack_Fun)
emitter.removeAllListeners(  [‘Event_Name’] )   //如果指定了事件名，就移除指定的，否则移除所有事件
```

一个案例：

```js
// 引入 events 核心模块
const EventEmitter = require('events')
// 创建一个继承 events 的类
class MyEmitter extends EventEmitter {}
// 实例化 events 对象
const myEmitter = new MyEmitter()
// 使用 on 方法创建一个 play 事件及响应方法
myEmitter.on('event1',()=>{
    console.log('event1 事件已被触发！')
})
// 使用 on 方法创建一个 play 事件及响应方法
myEmitter.on('event2',(a,b)=>{
    console.log(`event2 事件已被触发！并且接收到了 ${a}、${b}事件参数`)
})
// 使用 emit 方法触发事件
myEmitter.emit('event1')
// 传递事件参数
myEmitter.emit('event2','a','b')
```

运行结果如下：

```js
event1 事件已被触发！
event2 事件已被触发！并且接收到了 a、b 事件参数
```

## File System 模块

文件系统模块提供了标准的文件操作功能，而且，这些方法都提供异步和同步形式。强烈建议开发者使用异步版本，同步版本的文件操作会阻塞整个线程，直到操作完成为止。当然，在有必要的时候，我们必须要使用同步版本的方法。

异步操作形式始终拥有回调函数，且回调函数是最后一个参数。示例如下：

```js
const fs = require('fs');

fs.writeFile('test.txt', 'hello fs \n', (err) =>{
    if (err) { throw err;};
    console.log('成功创建并写入文件');
});
```

### stat 获得文件信息

`stat()`方法能获得文件的详细信息，如大小、创建时间、修改时间、拥有者、群组信息、文件类型信息等等。`stat()`方法的回调函数有两个参数，第一个是错误信息，第二个是 Stats 对象。

```js
fs.stat('test.txt', (err,stats) => {
    if (err) {
        console.log(err)
    } else {
        console.log(stats)
        console.log(`文件：${stats.isFile()}`)
    }
})
```

上述代码的运行结果如下：

```sh
Stats {
  dev: 39,
  mode: 33188,
  nlink: 1,
  uid: 1000,
  gid: 1000,
  rdev: 0,
  blksize: 4096,
  ino: 1413,
  size: 10,
  blocks: 8,
  atimeMs: 1528004135000,
  mtimeMs: 1528003341000,
  ctimeMs: 1528003341000,
  birthtimeMs: 1528003341000,
  atime: 2018-06-03T05:35:35.000Z,
  mtime: 2018-06-03T05:22:21.000Z,
  ctime: 2018-06-03T05:22:21.000Z,
  birthtime: 2018-06-03T05:22:21.000Z }
文件：true
```

### mkdir

mkdir() 方法能创建一个目录。该方法创建的目录默认值为`0o777`。

```js
fs.mkdir('log',(error)=>{
    if (error) {
        console.log(error)
    } else {
        console.log('目录创建成功')
    }
})
```

### 创建文件并写入内容

fs.writeFile() 方法可以创建文件并将内容写入文件中。方法用法如下：

```js
fs.writeFile(file, data[, options], callback)
```

### 读取内容

fs.readFile() 方法可以读取文件中的内容，用法如下：

```sh
fs.readFile(path[, options], callback)
```

其中`options` 可以用来指定读取时的编码方式（默认为 null）和打开方式（默认为 r）。

### 列出目录

fs.readdir() 方法可以读取指定目录中的文件名称，并以数组的形式返回。

### 重命名

fs.rename() 方法可以重命名文件、目录名称。

### 删除目录与文件

fs.rmdir() 用来删除目录，fs.unlink() 用来删除文件。

# buffer

Buffer 类是用来处理二进制数据，因为常用，所以直接放在全局变量，使用时无需 require

## 常用属性和方法

### Buffer.from(string [, encoding])

字符串转为 Buffer。

### buf.toString([encoding[, start[, end]]])

将 Buffer 转为字符串。

### buf.length

buf.length：返回 内存为此 Buffer 实例所申请的字节数，并不是 Buffer 实例内容的字节数

## stream

Stream 是 Node.js 提供的处理流数据的抽象接口。Node.js 中有许多对象是 stream 对象，例如 `request`。在 Unix 系统中流就是一个很常见也很重要的概念，从术语上讲流是对输入输出设备的抽象。

### 为什么要使用流

举个例子，如果直接读取一个非常大的文件（如视频文件），一次性读取则需要占用大量内存，这对于运行在服务器的程序来说是不可取的。因此就有了流，将大文件分段读取，每次读入以少部分进行处理，这样就可以在有限的服务器资源中并发处理一定规模的请求。Node.js 设计好了 stream 流，我们只需调用其接口，不用关心底层如何实现。

### Node.js 中的流类别

Node.js 内建了四类流， Readable 流、Writable 流、Duplex 流和 Transform 流的基类。另外如果觉得上述四类基类流不能满足需求，可以编写自己的扩充类流。

流是有方向的数据，按照流动方向可以分为三种流：

1. 设备流向程序：readable
1. 程序流向设备：writable
1. 双向：duplex、transform

当一个流同时面向生产者和消费者服务的时候我们会选择 Duplex，当只是对数据做一些转换工作的时候我们便会选择使用 Tranform。

NodeJS 关于流的操作被封装到了 Stream 模块，这个模块也被多个核心模块所引用。按照 Unix 的哲学：一切皆文件，在 NodeJS 中对文件的处理多数使用流来完成：

1. 普通文件
1. 设备文件（stdin、stdout）
1. 网络文件（http、net）

在 NodeJS 中所有的 Stream 都是 `EventEmitter` 的实例。

### 读取文件流

fs.createReadStream 方法可以创建读取文件信息的可读文件流。可读流是生产数据用来供程序消费的流。

```js
let fileReadStream = fs.createReadStream('data.json')

fileReadStream.on('data', (chuck) => {
    console.log(`接收到数据：${chuck.length}`)
})
```

### 可读流的事件

可读流的常用事件有：

1. data 当数据传入的时候就会触发；
1. error 当读取数据流时发生错误，就会触发；
1. end 数据全部读取完毕后触发。

### 写入流信息

先创建合适的可写流，再使用 write 方法写入到文件：

```js
let fileReadStream = fs.createReadStream('data.json')
let fileWriteStream = fs.createWriteStream('data-2.json')

fileReadStream.on('data', (chuck) => {
    console.log(`接收到数据：${chuck.length}`)
    fileWriteStream.write(chuck)
})
```

### 管道 pipe

pipe 管道，可以在写入流之前，进行中间环节的处理，如压缩、加密、改变编码方式等等。多个管道可以对接。

## HTPP 模块

Node.js 的 HTTP API 是非常底层的。 它只涉及流处理与消息解析。 它把一个消息解析成消息头和消息主体，但不解析具体的消息头或消息主体。

### http.ServerResponse 类

Response 类可以发送请求，接收请求。示例如下：

```js
const http = require('http')

var options = {
    protocol: 'http:',
    hostname: 'api.douban.com',
    port: '80',
    method: 'GET',
    path: '/v2/movie/top250'
}
var responseData = ''
var request = http.request(options, (response) => {
    console.log(response.statusCode)
    console.log(response.headers)
    // response.setEncoding = 'utf8'
    response.on('data', (chuck) => {
        // console.log(chuck)
        responseData += chuck
    })
    response.on('end', () => {
        JSON.parse(responseData).subjects.map((item) => {
            console.log(item.title)
        })
    })
})

request.on('error', (error) => {
    console.log(error)
})

request.end()
```

`http.request()`返回一个`http.ClientRequest`类的实例，该实例是一个可写的信息流。

### 创建服务器

使用`createServer()`方法可以创建 www 服务器，示例如下：

```js
'use strict'

const http = require('http')

var server = http.createServer()

server.on('request', (request, response) => {
    response.writeHead(200, {
        'Content-Type': 'text/html'
    })
    response.end(`<h1>hello :) </h1>`)
})

server.listen(8080)
```

其中`server`中的`request`事件发生于用户请求服务时。可以通过`server.listen()`方法指定 www 服务器运行端口。

## 扩展阅读资料

1. <https://nodejs.org/dist/latest-v8.x/docs/api/index.html>
1. <https://nodejs.org/dist/latest-v8.x/docs/api/events.html>
1. <https://nodejs.org/dist/latest-v8.x/docs/api/fs.html>
1. <https://github.com/jabez128/stream-handbook>
1. <https://nodejs.org/api/stream.html>
